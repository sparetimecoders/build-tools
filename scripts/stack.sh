#!/usr/bin/env bash

stack:scaffold() {
  local projectname="$1"
  true
}

stack:scaffold:dotfiles() {
  true
}

stack:validate() {
  true
}
